from math import cos, sin

from epics import caget, caput, ca
from evr_pulses import prepareEvr, startPulses

#i = magnitude * cos(angle)
#q = magnitude * sin(angle)

PREF = "LLRF-2"
PREFEVR = "TR-LLRF"
CHI = ":PI-I"
CHQ = ":PI-Q"
SPFIXED= "-FIXEDSPVAL"
EGU="-EGUCAL"
RAW="-RAWCAL"
UPD="-UPDCAL"

CHOUT = ":AI3-SMON-MAGCURR"
# ANG is not needed?
angout = 0

finished = False
Iegus = []
Iraws = []
Qegus = []
Qraws = []


def change_state(prefix, state):
    caput(prefix + ":MSGS", state)
    ca.poll(0.1)
    res = caget(prefix, as_string = True)
    if state == "RESET":
        return res == "RESETTING"
    return res == state

for state in ["RESET", "INIT", "ON"]:
    change_state(PREF, state)

prepareEvr(PREFEVR, 14, 2600, 2)

while (not finished):
    # ask for an input
    maginput = float(input("Enter the Magnitude value for Set Point: "))
    anginput = float(input("Enter the Angle value for Set Point: "))
    # set inputs on PVs
    iraw = maginput * cos(anginput)
    qraw = maginput * sin(anginput)
    caput(PREF+CHI+SPFIXED, iraw)
    caput(PREF+CHQ+SPFIXED, qraw)

    ca.poll(0.1)
    # acquire - with EVR or backplane signals? - 2 acquisitions to update inputs
    startPulses(PREFEVR)
    # read output values
    magout = caget(PREF+CHOUT)

    iraw = maginput * cos(anginput)
    qraw = maginput * sin(anginput)
    iegu = magout * cos(angout)
    qegu = magout * sin(angout)

    Iraws.append(iraw)
    Qraws.append(qraw)
    Iegus.append(iegu)
    Qegus.append(qegu)

    # check if finished
    cont = input("Continue[Y/n]? ")
    if (cont == "" or cont=="Y" or cont =="Y"):
        finished = False
    else:
        finished = True

# update calibration
caput(PREF+CHI+RAW, Iraws)
caput(PREF+CHQ+RAW, Qraws)
caput(PREF+CHI+EGU, Iegus)
caput(PREF+CHQ+EGU, Qegus)
caput(PREF+CHI+UPD, 1)
caput(PREF+CHQ+UPD, 1)
