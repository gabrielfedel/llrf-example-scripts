import sys
from math import sqrt

import numpy as np
from epics import caget

BAPosSmpPV = "%s:RFCErrRMSBeamStartSmp"
SPMagPV = "%s:SPTbl-Mag"
SPAngPV = "%s:SPTbl-Ang"
PIErrRMSMagBAPV = "%s:RFCErrRMS-BA-Cmp0"
PIErrRMSAngBAPV = "%s:RFCErrRMS-BA-Cmp1"

if len(sys.argv) != 2:
    print("A PV prefix is needed as argument")
    exit(1)

prefix = sys.argv[1]

# Read the needed values
BAPos = int(caget(BAPosSmpPV % prefix))
SPTblMag = caget(SPMagPV % prefix)
SPTblAng = caget(SPAngPV % prefix)

# PIERR values in EGU
PIErrRMSMagBA = caget(PIErrRMSMagBAPV % prefix)
PIErrRMSAngBA = caget(PIErrRMSAngBAPV % prefix)

## Get SPTbl value (EGU)
SPTblMagBA = SPTblMag[BAPos]
SPTblAngBA = SPTblAng[BAPos]

## Calculate stability
MagStability = (PIErrRMSMagBA / SPTblMagBA) * 100
AngStability = (PIErrRMSAngBA / SPTblAngBA) * 100



## Print results
print(MagStability)
print(AngStability)
