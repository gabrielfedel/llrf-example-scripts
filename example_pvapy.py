from time import sleep
import threading

import pvaccess # need to install pvapy


"""
This example monitor the PV LLRF-1:AI0 (waveform) and
everytime this PV changes a thread will be created to do some work
here the example will print the first value and update the LLRF-1:FF-PT0-I
and LLRF-1:FF-PT0-Q.
This program will execute until you stop with Ctrl+C
"""

PVPREFIX="LLRF-1"
PVDATANAME=PVPREFIX+":AI0"
PVOUTPREF=PVPREFIX+":FF-PT0"

OUTPUTDATA=16*[0.5]

workers = []

class WorkerThread(threading.Thread):
    def __init__(self, value):
        threading.Thread.__init__(self)
        self.value = value
        self.pv1Out = pvaccess.Channel(PVOUTPREF+"-I")
        self.pv2Out = pvaccess.Channel(PVOUTPREF+"-Q")
        self.pvOutWrt = pvaccess.Channel(PVOUTPREF+"-WRTBL")


    def run(self):
        print("Start worker", flush = True)
        if (self.value is not None and len(self.value) > 0):
            print(self.value[0][0])
            self.pv1Out.put(OUTPUTDATA)
            self.pv2Out.put(OUTPUTDATA)
            self.pvOutWrt.put(1)
            print("Data written on output channels", flush = True)
        else:
            print("Empty data", flush = True)
        print("Finish worker", flush = True)


def callback_func(pv):
    # do the proccesses - the best is to do outside the callback, so start a new thread
    workers.append(WorkerThread(list(pv.get().values())))
    workers[-1].start()


pv = pvaccess.Channel(PVDATANAME)
pv.subscribe("callback_func", callback_func)

# when you call startMonitor your callback_func will be called once
# and after that on each change your callback_func will be called again
pv.startMonitor()

while True:# keep running forever
    sleep(0.001)

pv.stopMonitor()

for w in workers:
    w.join()
