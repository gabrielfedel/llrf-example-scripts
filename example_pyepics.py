from time import sleep
import threading

from epics import PV, caget, caput # need to install pyepics

"""
This example monitor the PV LLRF-1:AI0 (waveform) and
everytime this PV changes a thread will be created to do some work
here the example will print the first value and update the LLRF-1:FF-PT0-I
and LLRF-1:FF-PT0-Q.
This program will execute until you stop with Ctrl+C
"""
PVPREFIX="LLRF-1"
PVDATANAME=PVPREFIX+":AI0"
PVOUTPREF=PVPREFIX+":FF-PT0"

OUTPUTDATA=16*[0.5]

workers = []

class WorkerThread(threading.Thread):
    def __init__(self, value):
        threading.Thread.__init__(self)
        self.value = value

    def run(self):
        print("Start worker", flush = True)
        if (self.value is not None and len(self.value) > 0):
            caput(PVOUTPREF+"-I", OUTPUTDATA)
            caput(PVOUTPREF+"-Q", OUTPUTDATA)
            caput(PVOUTPREF+"-WRTBL", 1)
            print("Data written on output channels", flush = True)
        else:
            print("Empty data", flush = True)
        print("Finish worker", flush = True)


def callback_func(pvname=None, value=None,  **kw):
        # do the proccesses - the best is to do outside the callback, so start a new thread
        workers.append(WorkerThread(value))
        workers[-1].start()


pv = PV(PVDATANAME, auto_monitor=True)

# When the callback is added it will be run once
pv.add_callback(callback_func)


while True:
    sleep(0.001)

for w in workers:
    w.join()
