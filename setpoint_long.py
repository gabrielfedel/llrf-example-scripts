import epics

Pfillratio=4.0
Pflattop=0.1
tfill=150
tflat=0.5
sampleFreq=epics.caget("LLRF1::FreqSystem")
neariqN=epics.caget("LLRF-010:DIG01:IQSmpNearIQ-N-RB")
Mag=[]
Pha=[]
time=[]
length=int((tfill+tflat)*sampleFreq/neariqN)
print(length)
data=0
for i in range(length):
    if 5>i:
        data=i*Pflattop/5
    elif (i>=5 and int(sampleFreq*tfill/neariqN)>=i):
        data=Pflattop*Pfillratio
    else:
        data=Pflattop
    Mag.append(data)
    Pha.append(0)
for j in range(length):
    time.append(j*(tfill+tflat)/length)

epics.caput("LLRF-010:DIG01:SPTbl-I", Mag)
epics.caput("LLRF-010:DIG01:SPTbl-Q", Pha)
print(Mag)
